//--- Class representing a user
export class User {
    public id: number;
    public firstName: string;
    public lastName: string;
    public creationTime: Date;


    constructor(firstName: string, lastName: string, creationTime: Date, id?: number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationTime = creationTime;
        this.id = id;
    }
}


//--- Class representing a user list
export class UserList {
    private users: User[]; // using array to store user list
    private searchOrder: number = 1;
    private sortkey: string = 'ID';

    constructor() {
        this.users = [];
    }

    getUsers(): User[] {
        this.sortIt();

        return this.users;
    }

    addUser(user: User) {
        this.users.push(user);
    }

    async deleteUser(userId: number): Promise<boolean> {
        let req = await fetch('/user/' + userId, {method: "DELETE"});
        let result = await req.json();
        if (!result || !result.success) {
            return false;
        }
        let index = this.users.findIndex((user: User) => user.id === userId);
        if (index === -1) {
            return false;
        }
        this.users.splice(index, 1);
        return true;
    }

    getUser(userId: number): User {
        // iterate through userList until user found (or end of list)
        for (const user of this.users) {
            if (userId === user.id) {
                return (user);  // leave function with "user" when found
            }
        }
        return null; // leave function with "null" when not found
    }

    async editUser(userId: number, firstName: string, lastName: string, groups: number[]): Promise<boolean> {
        let req = await fetch('/user/' + userId, {
            method: 'PUT', headers: {
                'Content-Type': 'application/json'
            }, body: JSON.stringify({firstName, lastName, groups})
        });
        let response = await req.json();
        if (!response || !response.user) {
            return false;
        }
        let found = this.users.find((user: User) => user.id === response.user.id);
        if (!found) {
            return false;
        }
        found.firstName = response.user.firstName;
        found.lastName = response.user.lastName;
        return true  // leave function with "False" when found
    }

    sortIt() {

        this.users.sort((a, b) => {
            let first;
            let second;
            if (this.sortkey === 'lastname') {
                first = a.lastName.toLowerCase();
                second = b.lastName.toLowerCase();
            } else if (this.sortkey === 'firstname') {
                first = a.firstName.toLowerCase();
                second = b.firstName.toLowerCase();
            } else {
                first = a.id;
                second = b.id;
            }

            if (first > second) {
                return this.searchOrder === 1 ? 1 : -1;
            }
            if (first < second) {
                return this.searchOrder === 1 ? -1 : 1;
            }
            return 0;
        });
    }

    toggleSort() {
        if (this.searchOrder === 1) {
            this.searchOrder = 0
        } else {
            this.searchOrder = 1
        }
    }

    setSortkey(key: string) {
        this.sortkey = key;
    }
}


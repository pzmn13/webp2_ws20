-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 07. Dez 2020 um 22:48
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `userman`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'User'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userlist`
--

CREATE TABLE `userlist` (
  `id` int(11) NOT NULL,
  `firstName` varchar(128) NOT NULL,
  `lastName` varchar(128) NOT NULL,
  `creationTime` varchar(32) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `userlist`
--

INSERT INTO `userlist` (`id`, `firstName`, `lastName`, `creationTime`, `username`, `password`) VALUES
(1, 'Patrik', 'KBBBBC', '2020-12-07', '', ''),
(2, 'EikeBBBBC', 'Z', '2020-12-07', '', ''),
(6, 'asd', 'asdf', '21:00:24', '', ''),
(8, 'asdf', 'asdf', '21:00:55', '', ''),
(10, 'asdf', 'asdf', '21:01:18', '', ''),
(11, 'asdf', 'asdf', '21:02:11', '', ''),
(13, 'asdf', 'asdf', '21:06:47', '', ''),
(14, 'asdf', 'asdff', '21:08:15', '', ''),
(15, 'ffffff', 'ffffff', '21:11:08', '', ''),
(16, 'fff', 'ffff', '21:13:38', '', ''),
(17, 'ffff', 'fff', '21:17:57', '', ''),
(22, 'test', 'test', '21:50:34', '', ''),
(23, 'test', 'test2', '21:50:43', '', ''),
(24, 'test4', 'test4', '21:50:48', '', ''),
(25, 'BB', 'CC', '22:40:00', '', ''),
(26, 'asdf', 'asdf', '22:41:56', '', ''),
(27, 'asdf', 'asdf', '22:43:00', '', ''),
(28, 'asdf', 'asdf', '22:43:36', '', ''),
(29, 'asdf', 'asdf', '22:46:45', 'asdf', '401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429080fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user_group`
--

INSERT INTO `user_group` (`id`, `user_id`, `group_id`) VALUES
(3, 2, 1),
(6, 6, 1),
(7, 6, 2),
(10, 8, 1),
(12, 10, 1),
(13, 11, 1),
(15, 15, 1),
(16, 15, 2),
(17, 16, 1),
(18, 16, 2),
(19, 17, 1),
(20, 17, 2),
(29, 22, 1),
(30, 23, 1),
(31, 23, 2),
(41, 1, 1),
(42, 25, 1),
(43, 25, 2),
(44, 28, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `userlist`
--
ALTER TABLE `userlist`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `userlist`
--
ALTER TABLE `userlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT für Tabelle `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `userlist` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

import {Group} from "./Group";

export class User {
	static idCounter: number = 1;
	id: number;
	firstName: string;
	lastName: string;
	groups: Group[];

	constructor(firstName: string, lastName: string, username: string, groups?: Group[], id?: number) { // Parameter id is optional
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
		this.groups = groups;
	}

}

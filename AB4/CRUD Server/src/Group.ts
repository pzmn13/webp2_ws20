export class Group {
	id: number;
	name: string;

	constructor(name: string, id: number) { // Parameter id is optional
		this.name = name;
		this.id=id;
	}
}

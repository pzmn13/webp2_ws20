import mysql = require ('mysql'); // handles database connections
import {Connection, MysqlError} from 'mysql';
import cryptoJS = require ('crypto-js');
import session = require('express-session');


const database: Connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'userman'
});

database.connect((err: MysqlError) => {
	if (err) {
		console.log('Database connection failed: ', err);
	} else {
		console.log('Database is connected');
	}
});

import express = require('express');

import {Request, Response} from 'express';
import {User} from './User';
import {Group} from "./Group";

let userList: User[] = [];

const app = express();

//--- session management -----------------------------------------------------
app.use(session({
	resave: true,
	saveUninitialized: true,
	secret: "234609236h9u6n23bip65nnbabawad2e1kl2451n2l5nkl125nn",
	name: "mySessionCookie",
	cookie: {maxAge: 20 * 60 * 1000}
}));

app.use(express.json());

app.use('/', express.static('../frontend'));

app.listen(8080, () => {
	console.log('Server started at http://localhost:8080');
});

app.post('/login', async (req: Request, res: Response) => {
	let password: string = req.body.password;
	let username: string = req.body.username;
	password = cryptoJS.SHA512(password);

	database.query('SELECT * FROM userlist WHERE username="' + username + '"', (err: MysqlError, result: any) => {
		if (err) {
			res.status(500).send({
				message: 'Database request failed: ' + err,
			});
		} else {
			if (result.length === 0) {
				res.status(404).send({
					message: 'no user found with username: ' + username,
				});
			} else {
				let user = result[0];
				if (user.password === password) {
					//erfolgreich eingeloggt
					req.session['user'] = user;
					res.status(200).send({success: true});
				} else {
					//passwort falsch
					res.status(403).send({
						message: 'Passwort falsch',
					});
				}
			}
		}
	});
});

app.get('/login', (req: Request, res: Response) => {
	if (req.session && req.session['user']) {
		res.status(200).send({success: true, user: req.session['user']});
	} else {
		res.status(200).send({success: false});
	}
});

app.post('/logout', (req: Request, res: Response) => {
	if (req.session && req.session['user']) {
		req.session['user'] = undefined;
		res.status(200).send({success: true});
	} else {
		res.status(200).send({success: false});
	}
});

/**
 * @api {post} /user Create a new user
 * @apiName PostUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {string} firstName The first name of the user to create
 * @apiParam {string} lastName The last name of the user to create
 *
 * @apiSuccess {User} user The created user object
 * @apiSuccess {string} message Message stating that the user has been created
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User created"
 *  }
 */
app.post('/user', async (req: Request, res: Response) => {
	// Read data from request body
	let firstName: string = req.body.firstName;
	let lastName: string = req.body.lastName;
	let password: string = req.body.password;
	let username: string = req.body.username;
	let groups: number[] = req.body.groups;

	if (!firstName || !lastName) {
		res.sendStatus(400);
		return;
	}

	password = cryptoJS.SHA512(password);


	let query: string = 'INSERT INTO userlist (creationTime, firstName, lastName, username, password) VALUES ("' +
		new Date().toLocaleTimeString() + '", "' + firstName + '", "' + lastName + '", "' + username + '", "' + password + '")';

	database.query(query, (err: MysqlError, result: any) => {
		if (err) {
			// Query could not been executed
			res.status(500).send({
				message: 'Database request failed: ' + err,
			});
		} else {
			// The user was created
			let valueString = '';
			groups.forEach((groupId: number) => {
				valueString += `(${result.insertId},${groupId}),`
			});
			valueString = valueString.slice(0, -1);
			database.query(`INSERT INTO user_group (user_id, group_id) VALUES ${valueString}`, () => {
				res.status(201).send({user: new User(firstName, lastName, username, [], result.insertId)});
			});
		}
	});

});

/**
 * @api {get} /user/:userId Request specified user
 * @apiName GetUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to fetch
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User fetched"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.get('/user/:userId', async (req: Request, res: Response) => {
	// Read data from request parameters
	let userId: number = req.params.userId;


	let query: string = "SELECT u.id as id, u.firstName as firstName, u.lastName as lastName,u.username as username, g.id as groupId, g.name as groupName " +
		"FROM userlist u " +
		"LEFT JOIN user_group ug ON ug.user_id = u.id " +
		"LEFT JOIN groups g ON ug.group_id = g.id " +
		"WHERE u.id =" + userId;

	database.query(query, (err: MysqlError, result: any) => {
		if (err) {
			res.status(500).send({
				message: 'Database request failed: ' + err,
			});
		} else {
			if (result.length === 0) {
				res.status(404).send({
					message: 'No user found',
				});
				return;
			}
			let groups: Group[] = [];
			result.forEach((entry: any) => {
				if (!entry.groupId) {
					return;
				}
				groups.push(new Group(entry.groupName, entry.groupId));
			});
			let user = new User(result[0].firstName, result[0].lastName, result[0].username, groups, result[0].id);
			res.status(201).send({user});
		}
	});

});

/**
 * @api {put} /user/:userId Update specified user
 * @apiName PutUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to update
 * @apiParam {string} firstName The first name of the user to update
 * @apiParam {string} lastName The last name of the user to update
 *
 * @apiSuccess {User} user The updated user object
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User updated"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.put('/user/:userId', (req: Request, res: Response) => {
	let userId: number = parseInt(req.params.userId);
	// Read data from request body
	let firstName: string = req.body.firstName;
	let lastName: string = req.body.lastName;
	let groups: number[] = req.body.groups;
	if (!userId || !firstName || !lastName || !groups) {
		res.sendStatus(400);
		return;
	}


	let query: string = 'UPDATE userlist SET lastName="' + lastName + '",firstName="' + firstName + '" WHERE id =' + userId;

	database.query(query, (err: MysqlError, result: any) => {
		if (err) {
			res.status(500).send({
				message: 'Database request failed: ' + err,
			});
		} else {
			database.query('DELETE FROM user_group WHERE user_id=' + userId, (err, result) => {
				if (err) {
					res.status(500).send({
						message: 'Database request failed: ' + err,
					});
				} else {
					if (groups.length === 0) {
						res.status(201).send({user: new User(firstName, lastName, '', [], userId)});
						return;
					}
					let valueString = '';
					groups.forEach((groupId: number) => {
						valueString += `(${userId},${groupId}),`
					});
					valueString = valueString.slice(0, -1);
					database.query(`INSERT INTO user_group (user_id, group_id) VALUES ${valueString}`, (err) => {
						if (err) {
							res.status(500).send({
								message: 'Database request failed: ' + err,
							});
							return;
						}
						res.status(201).send({user: new User(firstName, lastName, '', [], userId)});
					});
				}
			});
		}
	});
});

/**
 * @api {delete} /user/:userId Delete specified user
 * @apiName DeleteUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to delete
 *
 * @apiSuccess {string} message Message stating the requested user has been deleted
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "message":"User deleted"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.delete('/user/:userId', (req: Request, res: Response) => {
	let userId: number = parseInt(req.params.userId);
	if (!userId) {
		res.sendStatus(400);
		return;
	}
	let query: string = 'DELETE FROM userlist WHERE id =' + userId;

	database.query(query, (err: MysqlError, result: any) => {
		if (err) {
			res.status(500).send({
				message: 'Database request failed: ' + err,
			});
		} else {
			res.status(201).send({success: true});
		}
	});
});

/**
 * @api {get} /users Get all users
 * @apiName GetUsers
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiSuccess {User[]} userList The list of all users
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "userList":[
 *          {
 *              "id":1,
 *              "firstName":"Peter",
 *              "lastName":"Kneisel"
 *          },
 *          ...
 *      ],
 *      "message":"List of all users fetched"
 *  }
 */
app.get('/users', (req: Request, res: Response) => {
	let query = 'SELECT * FROM userlist';
	database.query(query, (err: MysqlError, result: any) => {
		if (err) {
			res.status(500).send({
				message: 'Database request failed: ' + err,
			});
		} else {
			res.status(201).send({users: result});
		}
	});
});

app.get('/groups', (req: Request, res: Response) => {
	const query: string = 'SELECT * FROM groups';

	database.query(query, (err: MysqlError, rows: any) => {
		if (err) {
			res.status(500).send({
				message: 'Database request failed : ' + err
			});
		} else {
			res.status(200).send({
				groups: rows,
				message: 'Successfully requested groups list',
			});
		}
	});
});

export {app, userList}; // For testing purposes. DO NOT DELETE



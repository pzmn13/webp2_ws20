//--- Class representing a user
export class User {
    private static userCounter: number = 1;  // unique user id
    public id: number;
    public firstName: string;
    public lastName: string;
    public creationTime: Date;
    public pet: Animal | null;

    public deletePet() {
        this.pet = null
    };


    constructor(firstName: string, lastName: string, creationTime: Date, pet: Animal) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationTime = creationTime;
        this.id = User.userCounter++;
        this.pet = pet;
    }
}


//--- Class representing a user list
export class UserList {
    private users: User[]; // using array to store user list

    constructor() {
        this.users = [];
    }

    private searchOrder: number = 1;

    private sortkey: string = 'ID';

    deletePet(userId: number): boolean {
        let user: User = this.users.find((user: User) => user.id === userId);
        if (!user) {
            return false;
        }
        if (user.pet) {
            user.deletePet();
        } else {
            return false;
        }
        return true;
    }

    getUsers(): User[] {
        this.sortIt();

        return this.users;
    }

    addUser(user: User) {
        this.users.push(user);
    }

    deleteUser(userId: number): boolean {
        let found: boolean = false;
        // Create a new array without the user to delete
        this.users = this.users.filter((user: User): boolean => {
            if (user.id === userId) {
                found = true;
                return false; // not to be copied
            } else {
                return true; // to be copied
            }
        });
        return found;
    }

    getUser(userId: number): User {
        // iterate through userList until user found (or end of list)
        for (const user of this.users) {
            if (userId === user.id) {
                return (user);  // leave function with "user" when found
            }
        }
        return null; // leave function with "null" when not found
    }

    editUser(userId: number, firstName: string, lastName: string, petName: string, petYearBorn: number, petSex: string): boolean {
        // iterate through userList until user found (or end of list)
        for (const user of this.users) {
            if (user.id === userId) {
                user.firstName = firstName;
                user.lastName = lastName;
                if (petName && petSex && petYearBorn) {
                    user.pet = new Animal(petName, petYearBorn, petSex);
                }
                return true;  // leave function with "true" when found
            }
        }
        return false  // leave function with "False" when found
    }

    sortIt() {

        this.users.sort((a, b) => {
            let first;
            let second;
            if (this.sortkey === 'lastname') {
                first = a.lastName.toLowerCase();
                second = b.lastName.toLowerCase();
            } else if (this.sortkey === 'firstname') {
                first = a.firstName.toLowerCase();
                second = b.firstName.toLowerCase();
            } else {
                first = a.id;
                second = b.id;
            }

            if (first > second) {
                return this.searchOrder === 1 ? 1 : -1;
            }
            if (first < second) {
                return this.searchOrder === 1 ? -1 : 1;
            }
            return 0;
        });
    }

    toggleSort() {
        if (this.searchOrder === 1) {
            this.searchOrder = 0
        } else {
            this.searchOrder = 1
        }
    }

    setSortkey(key: string) {
        this.sortkey = key;
    }
}


export class Animal {
    public name: string;
    public yearBorn: number;
    public sex: string;

    constructor(name: string, yearBorn: number, sex: string) {
        this.name = name;
        this.yearBorn = yearBorn;
        this.sex = sex;
    }
}

import express = require('express');

import {Request, Response} from 'express';
import {User} from './User';

let userList: User[] = [];

const app = express();

app.use(express.json());

app.listen(8080, () => {
	console.log('Server started at http://localhost:8080');
});

/**
 * @api {post} /user Create a new user
 * @apiName PostUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {string} firstName The first name of the user to create
 * @apiParam {string} lastName The last name of the user to create
 *
 * @apiSuccess {User} user The created user object
 * @apiSuccess {string} message Message stating that the user has been created
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User created"
 *  }
 */
app.post('/user', (req: Request, res: Response) => {
	// Read data from request body
	let firstName: string = req.body.firstName;
	let lastName: string = req.body.lastName;

	if (!firstName || !lastName) {
		res.sendStatus(400);
		return;
	}

	let user = new User(firstName, lastName);

	userList.push(user);
	res.status(201).send({user});
});

/**
 * @api {get} /user/:userId Request specified user
 * @apiName GetUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to fetch
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User fetched"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.get('/user/:userId', (req: Request, res: Response) => {
	// Read data from request parameters
	let userId: number = req.params.userId;
	// Search user in user list
	for (let user of userList) {
		if (user.id == userId) {
			res.status(200).send({
				user: user,
				message: 'User fetched'
			});
			// Terminate this route
			return;
		}
	}
	// The requested user was not found
	res.status(404).send({
		message: 'User not found'
	});
});

/**
 * @api {put} /user/:userId Update specified user
 * @apiName PutUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to update
 * @apiParam {string} firstName The first name of the user to update
 * @apiParam {string} lastName The last name of the user to update
 *
 * @apiSuccess {User} user The updated user object
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User updated"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.put('/user/:userId', (req: Request, res: Response) => {
	let userId: number = parseInt(req.params.userId);
	// Read data from request body
	let firstName: string = req.body.firstName;
	let lastName: string = req.body.lastName;
	if (!userId || !firstName || !lastName) {
		res.sendStatus(400);
		return;
	}
	let user = userList.find((user: User) => user.id === userId);
	if (!user) {
		res.sendStatus(404);
		return;
	}
	user.firstName = firstName;
	user.lastName = lastName;
	res.status(200).send({user});
});

/**
 * @api {delete} /user/:userId Delete specified user
 * @apiName DeleteUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to delete
 *
 * @apiSuccess {string} message Message stating the requested user has been deleted
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "message":"User deleted"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.delete('/user/:userId', (req: Request, res: Response) => {
	let userId: number = parseInt(req.params.userId);
	if (!userId) {
		res.sendStatus(400);
		return;
	}
	let index = userList.findIndex((user: User) => user.id === userId);
	if (index === -1) {
		res.sendStatus(404);
		return;
	}
	userList.splice(index, 1);
	res.sendStatus(200);
});

/**
 * @api {get} /users Get all users
 * @apiName GetUsers
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiSuccess {User[]} userList The list of all users
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "userList":[
 *          {
 *              "id":1,
 *              "firstName":"Peter",
 *              "lastName":"Kneisel"
 *          },
 *          ...
 *      ],
 *      "message":"List of all users fetched"
 *  }
 */
app.get('/users', (req: Request, res: Response) => {
	res.send({userList});
});

export {app, userList}; // For testing purposes. DO NOT DELETE

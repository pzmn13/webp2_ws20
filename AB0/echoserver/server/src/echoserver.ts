/*****************************************************************************
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 *****************************************************************************/
import express = require ("express");         // needed for routing
import { Request, Response } from "express";  // Request/Response from Express

/*****************************************************************************
 ***  Create server with handler function and start it                       *
 *****************************************************************************/
let router = express();
router.listen(8080, function () {
	console.log(`
	-------------------------------------------------------------
	  Ein einfacher Echoserver:
	  Gibt die Werte des Headers eines HTTP Request
	  und die Parameter (egal ob GET oder POST) zurück

	  Starte die beiden Clients im Browser
	     Post:  http://localhost:8080/post.html
	     Get:   http://localhost:8080/get.html
	  ... oder verwende client/clientTest.http
	-------------------------------------------------------------`);
});

/*****************************************************************************
 ***  Define JSON structure (with some attributes of the Request-object      *
 *****************************************************************************/
interface reqJSON {
	protocol    : string;
	ip          : string;
	method      : string;
	originalUrl : string;
}

/*****************************************************************************
 ***  Middleware routers                                                     *
 ****************************************************************************/
//--- urlencode Data if received ---------------------------------------------
router.use(express.urlencoded({ extended: false }));
//--- send static html-files from folder "/client" ---------------------------
let baseDir : string = __dirname + '/../..';
router.use("/",  express.static(baseDir + "/client/views"));

/*****************************************************************************
 ***  routers to handle requests, depending on methods and path              *
 *****************************************************************************/
router.all ("*", function (req: Request, res: Response ) {

  //--- Build (JSON-)Objects with information from request --------------------
  let header : reqJSON = {
    protocol    : req.protocol,
    ip          : req.ip,
    method      : req.method,
    originalUrl : req.originalUrl,
  };

  //--- set parameters depending on http-methode ------------------------------
  let parameters;
  if ( req.method == "POST" || req.method == "PUT"  ) {
  	parameters = req.body;
  }
  else if (req.method == "GET"  ) {
  	parameters = req.query;
  }

  //--- prepare and send response ---------------------------------------------
  res.status(200);                                            // set http status
  res.json( { Header: header, Parameters : parameters } );    // return json

});

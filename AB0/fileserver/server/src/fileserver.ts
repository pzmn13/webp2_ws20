/*****************************************************************************
 ***  Create HTTP server with handler function and start it                  *
 *****************************************************************************/
import express = require ("express");     // needed for routing

let router = express();
router.listen(8080, function () {
	console.log(`
    -------------------------------------------------------------
    Ein einfacher HTTP-Fileserver:
      Gibt eine Datei zurück, die in /client/views liegt    
    starte mit:
      http://localhost:8080/hallo.html oder
      http://localhost:8080/hello.html oder
      http://localhost:8080/huhu.html 
    -------------------------------------------------------------
  `);

});


/*****************************************************************************
 ***  Create router to send (static) files. Here: html-files of a client     *
 ****************************************************************************/
let baseDir : string = __dirname + '/../..';
router.use("/",    express.static(baseDir + "/client/views"));
router.use("/css", express.static(baseDir + "/client/css"));

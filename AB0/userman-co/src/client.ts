import {Animal, User, UserList} from './classes.js'

// Ist das die aktuelle?

/****************************************************************************
 * global variables                                                          *
 *****************************************************************************/
const userList: UserList = new UserList();
let userListFilter: User [] = userList.getUsers();


/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/
function addUser(event) {
	// Prevent the default behaviour of the browser (reloading the page)
	event.preventDefault();

	// Define JQuery HTML objects
	const addUserForm: JQuery = $('#add-user-form');
	const firstNameField: JQuery = $('#add-first-name-input');
	const lastNameField: JQuery = $('#add-last-name-input');

	const petField: JQuery = $('#add-pet-input-name');
	const petYearBornField: JQuery = $('#add-pet-input-yearBorn');
	const petSexField: JQuery = $('#add-pet-input-sex');


	// Read values from input fields
	const firstName: string = firstNameField.val().toString().trim();
	const lastName: string = lastNameField.val().toString().trim();

	const petName: string = petField.val().toString().trim();
	const petYearBorn : number = parseInt(petYearBornField.val().toString());
	const petSex: string = petSexField.val().toString().trim();

	// Check if all required fields are filled in
	if (firstName && lastName) {
		// Create new user and add it to userList
		if (petName && petYearBorn && petSex) {
			userList.addUser(new User(firstName, lastName, new Date(), new Animal(petName, petYearBorn, petSex)));
		} else {
			userList.addUser(new User(firstName, lastName, new Date(), null));
		}
		// Reset form by triggering "reset"-event
		addUserForm.trigger('reset');
		// Render message and user list
		renderMessage('User created');
		renderUserList(userList.getUsers());
		$('#buttonStatistics').show();

	} else {
		renderMessage('Not all mandatory fields are filled in');
	}
}
function deletePet(event) {

	event.preventDefault();

	const userId: number = $(event.currentTarget).data('user-id');

	if (userList.deletePet(userId)) {
		renderMessage('Pet deleted');
	} else {
		renderMessage('The Pet to be deleted could not be found');
	}
	renderUserList(userList.getUsers());
}

function editUser(event) {
	// Prevent the default behaviour of the browser (reloading the page)
	event.preventDefault();

	// Define JQuery HTML objects
	const editModal: JQuery = $('#edit-user-modal');
	const editUserForm: JQuery = $('#edit-user-form');
	const firstNameInput: JQuery = $('#edit-first-name-input');
	const lastNameInput: JQuery = $('#edit-last-name-input');
	const idHiddenInput: JQuery = $('#edit-id-input');

	const editPetNameInput: JQuery = $('#edit-pet-name-input');
	const editPetYearBorn: JQuery = $('#edit-year-born-input');
	const editPetSex: JQuery = $('#edit-pet-sex-input');


	// Read values from input fields
	const userId: number = Number(idHiddenInput.val().toString().trim());
	const firstName: string = firstNameInput.val().toString().trim();
	const lastName: string = lastNameInput.val().toString().trim();

	const petName: string = editPetNameInput.val().toString().trim();
	const petYearBorn: number = Number(editPetYearBorn.val().toString().trim());
	const petSex: string = editPetSex.val().toString().trim();


	if (firstName && lastName) {
		if (userList.editUser(userId, firstName, lastName, petName, petYearBorn, petSex)) {
			// Clear form and close modal
			editUserForm.trigger('reset');
			editModal.modal('hide');
			// Render message and user list
			renderMessage(`Successfully updated user ${firstName} ${lastName}`);
			renderUserList(userList.getUsers());
		} else { // The user could not be found, send error response
			renderMessage('The user to be updated could not be found');
		}
	}

	else { // Either firstName or lastName is missing
		renderMessage('Not all mandatory fields are filled in');
	}
}

function deleteUser(event) {
	// Get user id from button attribute 'data-user-id'
	const userId: number = $(event.currentTarget).data('user-id');
	// delete user and render message
	if (userList.deleteUser(userId)) {
		renderMessage('User deleted');
	} else {
		renderMessage('The user to be deleted could not be found');
	}
	// render user list
	renderUserList(userList.getUsers());

	if (userList.getUsers().length === 0) {
		$('#buttonStatistics').hide();
	}

}

function openEditUserModal(event) {
	// Get user id from button attribute 'data-user-id'
	const userId: number = $(event.currentTarget).data('user-id');

	// Search user in userList and show it in modal edit window
	let user: User = userList.getUser(userId);
	if (user !== null) { // user with userIds found in userList
		renderEditUserModal(user);
	} else { // user with userId not found in userList
		renderMessage('The selected user can not be found');
	}
}


function openStatisticsModal(event) {
	event.preventDefault();

	console.log("drinnen");
	const statisticsModal: JQuery = $('#statistics-modal');

	statistics();
	oldestPet();
	petCounter();

	statisticsModal.modal('show');

}

function petCounter() {

	let counter: number = 0;

	userList.getUsers().forEach((user: User) => {
		if (!user.pet) {

			return;
		}
		counter++
	});

	$('#petsCounter').html(counter.toString());
}


function arrAvg(arr: number[]) {

	return arr.reduce((a, b) => a + b, 0) / arr.length
}

function oldestPet() {

	let oldestPet = null;

	userList.getUsers().forEach((user: User) => {
		if (!user.pet) {
			return;
		}

		if (oldestPet === null) {
			oldestPet = user.pet.yearBorn;
			return;
		}
		if (user.pet.yearBorn < oldestPet) {
			oldestPet = user.pet.yearBorn;
		}
	});

	if (oldestPet === null) {
		$('#petAge').html('-');
		return;

	}

	let age = new Date().getFullYear() - oldestPet

	$('#petAge').html(age.toString());


}

function firstNameSort(event) {
	event.preventDefault();

	userList.setSortkey('firstname');

	searchbar();
}

function lastNameSort(event) {
	event.preventDefault();
	userList.setSortkey('lastname');
	searchbar();
}

function idSort(event) {
	event.preventDefault();
	userList.setSortkey('ID');
	searchbar();
}

function upDownSort(event) {
	event.preventDefault();
	userList.toggleSort();
	searchbar();

}


function statistics() {

	let ages: number[] = [];
	userList.getUsers().forEach((user: User) => {
		if (!user.pet) {
			return;
		}
		let age = new Date().getFullYear() - user.pet.yearBorn;
		ages.push(age);
	});


	let averageAge = arrAvg(ages);

	if (!isNaN(averageAge)) {
		$('#petAverage').html(averageAge.toString());
	} else {
		$('#petAverage').html('-');
	}


}

function searchbar(event = null) {

	if (event !== null) {
		event.preventDefault();
	}

	let search = $('#searchbar').val().toString().trim();

	if (search === '') {
		userListFilter = userList.getUsers();
		renderUserList(userList.getUsers());
		return;
	}
	userListFilter = userList.getUsers().filter((user: User) => {
		if (user.firstName.includes(search) || user.lastName.includes(search)) {

			return true;
		} else {
			return false;
		}
	});

	renderUserList(userListFilter);

}

/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
function renderMessage(message: string) {
	// Define JQuery HTML Objects
	const messageWindow: JQuery = $('#messages');

	// Create new alert
	const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

	// Add message to DOM
	messageWindow.append(newAlert);

	// Auto-remove message after 5 seconds (5000ms)
	setTimeout(() => {
		newAlert.alert('close');
	}, 5000);
}

function renderUserList(userList: User[]) {
	// Define JQuery HTML objects
	const userTableBody: JQuery = $('#user-table-body');

	// Delete the old table of users from the DOM
	userTableBody.empty();
	// For each user create a row and append it to the user table
	for (const user of userList) {
		// Create html table row element...
		const tableEntry: JQuery = $(`
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.pet ? user.pet.name : ''}</td>
                <td>${user.pet ? user.pet.yearBorn : ''}</td>
                <td>${user.pet ? user.pet.sex : ''}</td>
                <td>
                	${user.pet ? `<button class="btn btn-outline-dark btn-sm delete-pet-button mr-4" data-user-id="${user.id}" > 
                		<i class="fas fa-dog" aria-hidden="true"></i> </button>	`: ''}
                	
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-4" data-user-id="${user.id}" >
                        <i class="fa fa-pen" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);

		// ... and append it to the table's body
		userTableBody.append(tableEntry);
	}
}

function renderEditUserModal(user: User) {
	// Define JQuery HTML objects
	const editUserModal: JQuery = $('#edit-user-modal');
	const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
	const editFirstNameInput: JQuery = $('#edit-first-name-input');
	const editLastNameInput: JQuery = $('#edit-last-name-input');

	const editPetNameInput: JQuery = $('#edit-pet-name-input');
	const editPetYearBorn: JQuery = $('#edit-year-born-input');
	const editPetSex: JQuery = $('#edit-pet-sex-input');


	editIdInput.val(user.id);
	editFirstNameInput.val(user.firstName);
	editLastNameInput.val(user.lastName);

	if (user.pet && user.pet.name && user.pet.yearBorn && user.pet.sex) {
		editPetNameInput.val(user.pet.name);
		editPetYearBorn.val(user.pet.yearBorn);
		editPetSex.val(user.pet.sex);
	}


	// Show modal
	editUserModal.modal('show');
}


/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
	// Define JQuery HTML objects
	const addUserForm: JQuery = $('#add-user-form');
	const editUserForm: JQuery = $('#edit-user-form');
	const userTableBody: JQuery = $('#user-table-body');


// debugger; /* sets a breakpoint for browser-debuggung */

	// Register listeners
	addUserForm.on('submit', addUser);
	editUserForm.on('submit', editUser);
	userTableBody.on('click', '.edit-user-button', openEditUserModal);
	userTableBody.on('click', '.delete-user-button', deleteUser);

	userTableBody.on('click', '.delete-pet-button', deletePet);
	$('body').on('click', '.open-statistics-button', openStatisticsModal);
	$('body').on('keyup', '#searchbar', searchbar);
	$('body').on('click', '#firstNameSort', firstNameSort);
	$('body').on('click', '#lastNameSort', lastNameSort);
	$('body').on('click', '#idSort', idSort);
	$('body').on('click', '#upDownSort', upDownSort);
});


import express = require('express');
import bodyParser = require('body-parser');
import * as path from 'path';

import {NumberGuesser} from "./NumberGuesser";

const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));
app.use('/node_modules', express.static(path.resolve(__dirname, '..', '..', 'node_modules')));

app.listen(8080, () => {
    console.log('Server started at http://localhost:8080');
    new NumberGuesser(app);
});
let formGuess: JQuery = $('#form-guess');
let formCheat: JQuery = $('#form-cheat');
let formReset: JQuery = $('#form-reset');

let alertErrorField: JQuery = $('#alert-error');
let alertSuccessField: JQuery = $('#alert-success');

formGuess.on('submit', onFormGuessSubmit);
formCheat.on('submit', onFormCheatSubmit);
formReset.on('submit', onFormResetSubmit);


async function onFormGuessSubmit(evt) {
    evt.preventDefault();
    let guessNumber = $('#input-guess').val();
    if (!guessNumber) {
        logMessage('Kein Wert eingegeben.', 'error');
        return;
    }
    const response = await fetch(`/guess/${guessNumber}`);
    let result = await response.json();

    logMessage(`${result.event}, ${result.response}`, result && result.success ? 'success' : 'error');
}

async function onFormCheatSubmit(evt) {
    evt.preventDefault();
    let pw = $('#input-cheat-pw').val();
    if (!pw) {
        logMessage('Kein Wert eingegeben.', 'error');
        return;
    }
    const response = await fetch(`/cheat`, {
        method: "POST", body: JSON.stringify({pw}), headers: {
            'Content-Type': 'application/json'
        },
    });
    let result = await response.json();
    if (result.success) {
        logMessage(`Password ist korrekt, aktuelle Zahl ist ${result.currentNumber}`);
    } else {
        logMessage(`Passwort nicht korrekt`, 'error');
    }
}

async function onFormResetSubmit(evt) {
    evt.preventDefault();
    let min = $('#input-reset-min').val();
    let max = $('#input-reset-max').val();
    const response = await fetch(`/reset`, {
        method: "POST", body: JSON.stringify({min, max}), headers: {
            'Content-Type': 'application/json'
        },
    });
    let result = await response.json();
    if (result.success) {
        logMessage(`Es wurde eine neue Zahl zwischen ${result.min} und ${result.max} generiert!`);
    } else {
        logMessage(`Ein Fehler ist aufgetreten.`, 'error');
    }
}

function logMessage(message: string, type: string = 'success') {
    alertErrorField.hide();
    alertSuccessField.hide();
    if (type === 'error') {
        alertErrorField.html(message);
        alertErrorField.show();
    } else {
        alertSuccessField.html(message);
        alertSuccessField.show()
    }
}

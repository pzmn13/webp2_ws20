import {Request, Response} from 'express';

const MIN_DEFAULT = 1;
const MAX_DEFAULT = 10;

export class NumberGuesser {
    private randomNumber: number = null;
    private password: string = 'asdfgh';

    constructor(private app: any) {
        this.resetRandomNumber();
        this.registerRoutes();
    }

    private random(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min) + min);
    }

    private resetRandomNumber(min: number = MIN_DEFAULT, max: number = MAX_DEFAULT) {
        this.randomNumber = this.random(min, max);
        console.log(`NumberGuesser initialized with number ${this.randomNumber}`);
    }

    private registerRoutes() {
        this.app.get('/guess/:guess', (req: Request, res: Response) => {
            // Read data from request parameters
            let guessNumber: number = parseInt(req.params.guess);
            if (isNaN(guessNumber)) {
                // The requested user was not found
                res.status(200).send({
                    event: 'Gegebenes Argument ist keine Zahl',
                    response: 'Please guess a number!',
                    success: false
                });
                return;
            }

            if (guessNumber < this.randomNumber) {
                res.status(200).send({
                    event: 'Gegebene Zahl ist kleiner als die Gesuchte',
                    response: 'Your guess was too low!',
                    success: false
                });
                return;
            }

            if (guessNumber > this.randomNumber) {
                res.status(200).send({
                    event: 'Gegebene Zahl ist größer als die Gesuchte',
                    response: 'Your guess was too high!',
                    success: false
                });
                return;
            }

            res.status(200).send({
                event: 'Gegebene Zahl ist die Gesuchte',
                response: 'You got it!',
                success: true
            });

        });

        this.app.post('/reset', (req: Request, res: Response) => {
            // Read data from request parameters
            let minNumber: number = parseInt(req.body.min);
            let maxNumber: number = parseInt(req.body.max);
            if (isNaN(minNumber) || isNaN(maxNumber)) {
                this.resetRandomNumber();
                res.status(200).send({
                    success: true,
                    min: MIN_DEFAULT,
                    max: MAX_DEFAULT
                });
                return;
            }
            this.resetRandomNumber(minNumber, maxNumber);
            res.status(200).send({
                success: true,
                min: minNumber,
                max: maxNumber
            });
        });

        this.app.post('/cheat', (req: Request, res: Response) => {
            let password: string = req.body.pw;
            console.log(req.body);
            console.log(password);
            if (password && password === this.password) {
                res.status(200).send({
                    success: true,
                    currentNumber: this.randomNumber
                });
                return;
            }
            res.status(200).send({
                success: false
            });
        });
    }

}
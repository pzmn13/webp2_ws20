/*****************************************************************************/
/*  Import some module from node.client (see: https://nodejs.org/api)        */
/*****************************************************************************/
import * as express         from "express";   // import EXPRESS
import {Request, Response}  from "express";   // import from EXPRESS


/*****************************************************************************
 * intialize router                                                          *
 *****************************************************************************/
let app = express();
app.listen(8080, "localhost", function () {
	/*--- Provide some useful information on console  -------------------------*/
	console.log(`
	-------------------------------------------------------------
	  Adder webservice - using ajax
	  Adds three numbers provides as Post-Parameters

	  Server started  in localhost:8080
	  call http://localhost:8080/adder.html	
	-------------------------------------------------------------
	`);
});


/*****************************************************************************/
/* Middleware routers                                                        */
/*****************************************************************************/
//---parse json Body-Data if received -----------------------------------------
app.use(express.json());

//--- route static files ------------------------------------------------------
let baseDir : string = __dirname + '/../..';  // get rid of /server/src
app.use("/",       express.static(baseDir + "/client/views"));
app.use("/src",    express.static(baseDir + "/client/src"));
app.use("/jquery", express.static(baseDir + "/client/node_modules/jquery/dist"));


/*****************************************************************************/
/*  routers to handle requests, depending on methods                         */
/*****************************************************************************/
app.post("/add", (req: Request, res: Response) => {
  let result : number = Number(req.body.a) + Number(req.body.b) + Number(req.body.c);
  res.status(200);
  res.json( { result: result } );
});

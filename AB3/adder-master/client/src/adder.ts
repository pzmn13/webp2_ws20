/** **************************************************************************
 *  send parameters to server via jQuery-AJAX-Object and shows result        *
 *****************************************************************************/
$( function() {

  //--- register Callback-function when button with id = "btn" clicked --------
  $('#btn').on("click", () => {

		let data: string = JSON.stringify( {a: $('#a').val(), b: $('#b').val(), c: $('#c').val() });

    //---- set up ajax-request ------------------------------------------------
    $.ajax({                // set up ajax request
      url         : 'http://localhost:8080/add',
      type        : 'POST',    // POST-request for CREATE
			contentType : 'application/json',
			data        : data,
      dataType    : 'json',    // expecting json
      success     : (data) => { $('#result').html(data.result) },
      error       : (jqXHR, Status, error) => { alert(error) }
    });
  });

});